//project proxy ini

//
#include <stdio.h>
#include <map>
#include <math.h>
#include <iostream>
#include <map>
#include <string>
#include <chrono>
#include <thread>
#include <mutex>

class subs
{
	public:
		virtual void zapyt() const = 0;
};

class rsub : public subs 
{
	public:
		void zapyt() const override
		{
			std::cout << "�������� ������: ������� �����.\n";
		}
};

class rsub2 : public subs
{
public:
	void zapyt() const override
	{
		std::cout << "�������� ������2: ������� �����.\n";
	}
};

class rsub3 : public subs
{
public:
	void zapyt() const override
	{
		std::cout << "�������� ������3: ������� �����.\n";
	}
};

class proksi : public subs 
{
	
	private:
		rsub* r_sub_;
		bool perev_dpustup_2() const 
		{
			// ���������� �������� ����� ���� ���
			std::cout << "�����i: �����i��� ��i������ ������� ��� ������.\n";
			return true;
		}
		void log_dostup() const 
		{
			std::cout << "�����i: �����i��� ��� �������.\n";
		}
		//
		//
		//
	public:
		proksi(rsub* r_sub) : r_sub_(new rsub(*r_sub)) 
		{
		}

		~proksi() 
		{
			delete r_sub_;
		}
	
		void zapyt() const override 
		{
			if (this->perev_dpustup_2()) 
			{
				this->r_sub_->zapyt();
				this->log_dostup();
			}
		}
};

void CodeTempRenew() 
{
	//
}


void klientskyikod(const subs& subject)
{
	subject.zapyt();
}

int main()
{
	setlocale(LC_CTYPE, "ukr");
	std::cout << "����� 1: ������ �i� � i� ������� �� ������:\n";
	rsub* rs1 = new rsub;
	klientskyikod(*rs1);
	//
	std::cout << "\n";
	std::cout << "����� 2: ������ ���������� �� � ���� �i� i� ������� �� ������:\n";

	/*
	std::cout << "\n";
	std::cout << "����� 3: ������ ���������� �� � ���� �i� i� ������� �� ������:\n";
	*/
	proksi* proxy = new proksi(rs1);
	klientskyikod(*proxy);
	//freespacethen
	delete rs1;
	delete proxy;
	return 0;
}