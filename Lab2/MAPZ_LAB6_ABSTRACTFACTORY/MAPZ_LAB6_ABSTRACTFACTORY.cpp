#include <iostream>
#include <map>
#include <string>
#include <chrono>
#include <thread>
#include <mutex>

//
int counter = 0;
class ProdA {
public:
	virtual ~ProdA() {};
	virtual std::string UsefulFunctionA() const = 0;
};

class Fishrotlvl1 : public ProdA {
public:
	std::string UsefulFunctionA() const override {
		return "������� 1";
	}
};

class Fishrotlvl2 : public ProdA {
	//todo override
	std::string UsefulFunctionA() const override {
		return "������� 2";
	}
};

/*
������� ������� ���� �� ��'������ �������� �� �������� ����� ��� 
(����� �������1 -> ������� 1, ��� �� ������� 2-> ������� 1)
 */
class AProdB {
	
	public:
		virtual ~AProdB() {};
		virtual std::string UsefulFunctionB() const = 0;
		int id = 0;;
		virtual std::string AnotherUsefulFunctionB(const ProdA& collaborator) const = 0;
};


class CProdB1 : public AProdB {
public:
	std::string UsefulFunctionB() const override {
		return "��������� ������� 1:";
	}
	
	std::string AnotherUsefulFunctionB(const ProdA& collaborator) const override {
		const std::string result = collaborator.UsefulFunctionA();
		return "����� 1 �����������: " + result + "";
	}
};

class CProdB2 : public AProdB {
public:
	std::string UsefulFunctionB() const override {
		return "��������� ������� 2:";
	}
	
	std::string AnotherUsefulFunctionB(const ProdA& collaborator) const override {
		const std::string result = collaborator.UsefulFunctionA();
		return "����� 2 �����������: " + result + "";
	}
};

class abstractnafabryka {
public:
	virtual ProdA* CreateProdA() const = 0;
	virtual AProdB* CreateProdB() const = 0;
};

class FabrykaA : public abstractnafabryka {
public:
	ProdA* CreateProdA() const override {
		return new Fishrotlvl1();
	}
	AProdB* CreateProdB() const override {
		return new CProdB1();
	}
};

class FabrykaB : public abstractnafabryka {
public:
	ProdA* CreateProdA() const override {
		return new Fishrotlvl2();
	}
	AProdB* CreateProdB() const override {
		return new CProdB2();
	}
};

void clientskyikod(const abstractnafabryka& factory) {
	const ProdA* product_a = factory.CreateProdA();
	const AProdB* product_b = factory.CreateProdB();
	std::cout << product_b->UsefulFunctionB() << "\n";
	std::cout << product_b->AnotherUsefulFunctionB(*product_a) << "\n";
	delete product_a;
	delete product_b;
}

int main() 
{
	setlocale(LC_CTYPE, "ukr");
	std::cout << "���� ���i� ��� ����� �������:\n";
	FabrykaA* f1 = new FabrykaA();
	clientskyikod(*f1);
	delete f1;
	std::cout << std::endl;
	std::cout << "���� ���i� ��� ����� �������:\n";
	FabrykaB* f2 = new FabrykaB();
	clientskyikod(*f2);
	delete f2;
	return 0;
}
