#include <iostream>
#include <chrono>
#include <thread>

//do not use semaphore
//mutex for 1 -> 0 state
/*

		class single
{
	const int counter = 0;
	float a[2];
	protected:
		
		static single* singleton_;
		std::vector

single(single& other) = delete;

void operator=(const single&) = delete;
new=getinstatnce//
static single* getstate(const std::string& value);
std::string value() const {
	return value_;
}
};
*/
class single
{
	const int counter = 0;
	float a[2];
	protected:
		single(const std::string value) : value_(value)
		{
		}
		//
		static single* singleton_;
		//
		std::string value_;

	public:

		/**
		 ���������� ����������
		 */
		single(single& other) = delete;
		/**
		 * ѳ������ ��� ���������
		 */
		void operator=(const single&) = delete;
		/**
		 �����,��������� ������ �� �������� � ��������� ��'����
		 */
		static single* getstate(const std::string& value);
		std::string value() const {
			return value_;
		}
};

single* single::singleton_ = nullptr;;

single* single::getstate(const std::string& value)
{
	if (singleton_ == nullptr) {
		singleton_ = new single(value);
	}
	return singleton_;
}

void playerivan() {
	//������������
	std::this_thread::sleep_for(std::chrono::milliseconds(1000));
	single* singleton = single::getstate("����� I���");
	std::cout << singleton->value() << "\n";
}

void playerroman() {
	// 
	std::this_thread::sleep_for(std::chrono::milliseconds(1000));
	single* singleton = single::getstate("����� �����");
	std::cout << singleton->value() << "\n";
}


int main()
{
	setlocale(LC_CTYPE, "ukr");
	std::cout << "��i� �� ���/�������i�. ���� i���� ��i��������, ���i �i������ ���������\n" <<
		"���� i���� �i��i, ���� ��'���i� ���i����,\n �������� ������������ �i�������,\n � �� ��������� �� ��� �� ��'��� (�� �� � ����������e����) \n\n" <<
		"���������:\n";
	std::thread t1(playerivan);
	std::thread t2(playerroman);
	t1.join();
	t2.join();
	//
	return 0;
}