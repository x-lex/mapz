#include <iostream>
#include <map>
#include <string>
#include <chrono>
#include <thread>
#include <mutex>
#include <stdarg.h>
#include <stdbool.h>


//������� ���������������

class safesingle
{
	private:
		static safesingle* pstate;
		static std::mutex sinc;

	protected:
		safesingle(const std::string value) : value_(value)
		{
		}
		~safesingle() {}
		std::string value_;
		//���������� � �������������
	public:
		safesingle(safesingle& other) = delete;
		void operator=(const safesingle&) = delete;
		//
		static safesingle* getstate(const std::string& value);
		std::string value() const {
			return value_;
		}
};

safesingle* safesingle::pstate{ nullptr };
std::mutex safesingle::sinc;

safesingle* safesingle::getstate(const std::string& value)
{
	if (pstate == nullptr)
	{
		//
		//
		//
		std::lock_guard<std::mutex> lock(sinc);
		if (pstate == nullptr)
		{
			pstate = new safesingle(value);
		}
	}
	return pstate;
}

void playerivan() {
	std::this_thread::sleep_for(std::chrono::milliseconds(1000));
	safesingle* singleton = safesingle::getstate("����� I���");
	std::cout << singleton->value() << "\n";
}
//
void playerroman() {
	std::this_thread::sleep_for(std::chrono::milliseconds(1000));
	safesingle* singleton = safesingle::getstate("����� �����");
	std::cout << singleton->value() << "\n";
}

int main()
{
	setlocale(LC_CTYPE, "ukr");
	std::cout << "��i� �� ���/�������i�. ���� i���� ��i��������, ���i �i������ ���������\n" <<
		"���� i���� �i��i, ���� ��'���i� ���i����,\n �������� ������������ �i�������,\n � �� ��������� �� ��� �� ��'��� (�� �� � ����������e����) \n\n" <<
		"���������:\n";
	/*
	���������� ������� � ������� �� �������
	*/
	std::thread t1(playerivan);
	std::thread t2(playerroman);
	/*
	std::thread t3(playerivan);
	std::thread t4(playerroman);
	*/
	t1.join();
	t2.join();
	/*
	t3.join();
	t4.join();
	*/
	return 0;
}