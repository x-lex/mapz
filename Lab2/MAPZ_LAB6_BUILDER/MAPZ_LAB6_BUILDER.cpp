#include <iostream>
#include <map>
#include <string>
#include <chrono>
#include <thread>
#include <mutex>
#include <vector>

/*
 todo class client
*/
class produktA {
	public:
		std::vector<std::string> parts_;
		/*
		������������ �� ����� �� ����
		���������� ������� ����� ������

		*/
		void komplektacia()const {
			std::cout << "����������i�: ";
			for (size_t i = 0; i < parts_.size(); i++) {
				if (parts_[i] == parts_.back()) {
					std::cout << parts_[i];
				}
				else {
					std::cout << parts_[i] << ", ";
				}
			}
			std::cout << "\n\n";
		}
};
/*
class Builder {
	public:
		virtual ~Builder() {}
		void set_builder(Builder* builder) {
			this->builder = builder;
		}

		void BuildMinimalViableProduct() {
			this->builder->ProducePartA();
		}

		void BuildFullFeaturedProduct() {
			this->builder->ProducePartA();
			this->builder->ProducePartB();
			this->builder->ProducePartC();
		}
		
};
*/

class budivelnik {
	public:
		virtual ~budivelnik() {}
		virtual void sethooks() const = 0;
		virtual void eletricshocker() const = 0;
		virtual void setwire() const = 0;
};

class budivelnik1 : public budivelnik {
	private:

		produktA* product;

	
	public:

		budivelnik1() {
			this->onovyty();
		}

		~budivelnik1() {
			delete product;
		}

		void onovyty() {
			this->product = new produktA();
		}
		/**
		 г��� ��������� �������
		 */

		void sethooks()const override {
			this->product->parts_.push_back("�������� ���i�");
		}
		/*
		void rubin()const override {
			this->product->parts_.push_back("����");
		}
		*/

		void eletricshocker()const override {
			this->product->parts_.push_back("������������");
		}

		void setwire()const override {
			this->product->parts_.push_back("�������� �����i��");
		}

		produktA* GetProduct() {
			produktA* result = this->product;
			this->onovyty();
			return result;
		}
};

class Director {
	/*
	��� ����������
	 */
	private:
		budivelnik* builder;

	public:

		void set_builder(budivelnik* builder) {
			this->builder = builder;
		}

		void BuildMinimalViableProduct() {
			this->builder->sethooks();
		}

		void BuildFullFeaturedProduct() {
			this->builder->sethooks();
			this->builder->eletricshocker();
			this->builder->setwire();
		}
};

void klientskyikod(Director& director)
{
	budivelnik1* builder = new budivelnik1();
	director.set_builder(builder);
	std::cout << "������ �������:\n";
	director.BuildMinimalViableProduct();

	/*
	std::cout << "��������� �������:\n";
	builder->ProducePartA();
	builder->ProducePartC();
	p = builder->GetProduct();
	p->ListParts();
	delete p;
	//builder->
	delete builder;
	*/
	produktA* p = builder->GetProduct();
	p->komplektacia();
	delete p;

	std::cout << "��i������ ����������:\n";
	director.BuildFullFeaturedProduct();

	p = builder->GetProduct();
	p->komplektacia();
	delete p;

	// ������ ����� ������� ��� ����� ������
	std::cout << "��������� �������:\n";
	builder->sethooks();
	builder->setwire();
	p = builder->GetProduct();
	p->komplektacia();
	delete p;
	//builder->
	delete builder;
}

int main() {
	setlocale(LC_CTYPE, "ukr");
	Director* director = new Director();
	klientskyikod(*director);
	delete director;
	return 0;
}