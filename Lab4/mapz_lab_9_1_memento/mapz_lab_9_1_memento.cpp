#include <iostream>
#include <map>
#include <string>
#include <chrono>
#include <thread>
#include <mutex>
#include <vector>
#include <ctime>
#define _CRT_SECURE_NO_WARNINGS
#pragma warning(disable:4996)


class shablonmem 
{
	public:
		virtual std::string GetName() const = 0;
		virtual std::string date() const = 0;
		virtual std::string state() const = 0;
};

class mem1 : public shablonmem 
{
	private:
		std::string stan_1;
		std::string temp_time;

	public:
		mem1(std::string state) : stan_1(state) 
		{
			this->stan_1 = state;
			std::time_t now = std::time(0);
			this->temp_time = std::ctime(&now);
		}
		std::string get_temp_state() const override 
		{
			return this->stan_1;
		}

		std::string get_name() const override 
		{
			return this->temp_time + " / (" + this->stan_1.substr(0, 9) + "...)";
		}
		std::string get_data_time() const override 
		{
			return this->temp_time;
		}
};

class temp_origin 
{
	private:
		std::string stan_1;

		std::string random_fish_score(int length = 10)
		{
			const char alphanum[] =
				"1234567890";
			int stringLength = sizeof(alphanum) - 1;

			std::string random_string;
			for (int i = 0; i < length; i++) 
			{
				random_string += alphanum[std::rand() % stringLength];
			}
			return random_string;
		}

	public:
		temp_origin(std::string state) : stan_1(state) 
		{
			std::cout << "�����: �������� ������� : " << this->stan_1 << "\n";
		}
	
		void player_activity() 
		{
			std::cout << "�����: ������ ����. ������� ���� ��i�������...\n";
			this->stan_1 = this->random_fish_score(5);
			std::cout << "�����: �������� ������� ��i����� : " << this->stan_1 << "\n";
		}

		shablonmem* temp_save_state() 
		{
			return new mem1(this->stan_1);
		}
	
		void temp_renew_player_score(shablonmem* memento) 
		{
			this->stan_1 = memento->state();
			std::cout << "�����: �������� ������� ��i����� : " << this->stan_1 << "\n";
		}
};


class temp_service_renewscore 
{
	
	private:
		std::vector<shablonmem*> mem_S;
		temp_origin* or1;

	public:
		temp_service_renewscore(temp_origin* originator) : or1(originator) 
		{
			this->or1 = originator;
		}

		void temp_do_score_backup() 
		{
			std::cout << "\n������: ����i�� ��������� ����...\n";
			this->mem_S.push_back(this->or1->temp_save_state());
		}
		void go_state_back() 
		{
			if (!this->mem_S.size()) 
			{
				return;
			}
			shablonmem* memento = this->mem_S.back();
			this->mem_S.pop_back();
			std::cout << "������: �i������� ��������i� ����: " << memento->GetName() << "\n";
			try 
			{
				this->or1->temp_renew_player_score(memento);
			}
			catch (...) 
			{
				this->go_state_back();
			}
		}
		void temp_show_all_scores() const 
		{
			std::cout << "������: �������� ������ ���������� ��i��i� ������i�:\n";
			for (shablonmem* memento : this->mem_S)
			{
				std::cout << memento->GetName() << "\n";
			}
		}
};

void klientskyikod() 
{
	temp_origin* originator = new temp_origin("00000.");
	temp_service_renewscore* caretaker = new temp_service_renewscore(originator);
	caretaker->temp_do_score_backup();
	originator->player_activity();
	caretaker->temp_do_score_backup();
	originator->player_activity();
	caretaker->temp_do_score_backup();
	originator->player_activity();
	std::cout << "\n";
	caretaker->temp_show_all_scores();
	std::cout << "\n��i���: �i������� ��������i� ����\n\n";
	caretaker->go_state_back();
	std::cout << "\n��i���: �i������� ��������i� ����\n\n";
	caretaker->go_state_back();

	delete originator;
	delete caretaker;
}

int main() 
{
	setlocale(LC_CTYPE, "ukr");
	std::srand(static_cast<unsigned int>(std::time(NULL)));
	klientskyikod();
	return 0;
}