//project state init
#include <vector>
#include <ctime>
#include <iostream>
#include <typeinfo>
#include <iostream>
#include <map>

#include <string>
#include <chrono>
#include <thread>
#include <mutex>

#include <stdarg.h>


class temp_states;

class temp_state_key 
{
	protected:
		temp_states* temp_context_state;

	public:
		virtual ~temp_state_key() 
		{
		}

		void set_context(temp_states* context) 
		{
			this->temp_context_state = context;
		}

		virtual void oper1() = 0;
		virtual void oper2() = 0;
};


class temp_states 
{
	private:
		temp_state_key* state_main;

	public:
		temp_states(temp_state_key* state) : state_main(nullptr) 
		{
			this->perehid_stanu(state);
		}
		~temp_states() 
		{
			delete state_main;
		}
		
		void perehid_stanu(temp_state_key* state) 
		{
			std::cout << "���i� ���: �����i� ����� �� " << typeid(*state).name() << ".\n";
			if (this->state_main != nullptr)
				delete this->state_main;
			this->state_main = state;
			this->state_main->set_context(this);
		}
		
		void zapyt1() 
		{
			this->state_main->oper1();
		}
		void zapyt2() 
		{
			this->state_main->oper2();
		}
};


class Stan_Ready : public temp_state_key 
{
	public:
		void oper1() override;

		void oper2() override 
		{
			std::cout << "ConcreteStateA ��������� ����� 2.\n";
		}
};

class Stan_Working : public temp_state_key 
{
	public:
		void oper1() override
		{
			std::cout << "ConcreteStateB ��������� ����� 1.\n";
		}
		void oper2() override 
		{
			std::cout << "ConcreteStateB ��������� ����� 2.\n";
			std::cout << "ConcreteStateB ��i��� ����.\n";
			this->temp_context_state->perehid_stanu(new Stan_Ready);
		}
};

/*
void Stan_Ready::oper3()
{
	{
		this->temp_context_state->perehid_stanu(new Stan_Waiting);
	}
}
*/

void Stan_Ready::oper1() 
{
	{
		std::cout << "ConcreteStateA ��������� ����� 1.\n";
		std::cout << "ConcreteStateA ��i��� ����.\n";

		this->temp_context_state->perehid_stanu(new Stan_Working);
	}
}
void _temp_klient()
{
	temp_states* context = new temp_states(new Stan_Ready);
	//context->zapyt1();
	//context->zapyt2();
	//delete context;
}

void klientskyi_kod() 
{
	temp_states* kontekst_servera = new temp_states(new Stan_Ready);
	kontekst_servera->zapyt1();
	kontekst_servera->zapyt2();
	//kontekst_servera->zapyt3();
	//kontekst_servera->zapyt4();
	delete kontekst_servera;
}

int main() 
{
	setlocale(LC_CTYPE, "ukr");
	//setlocale(LC_CTYPE, "RUS");
	klientskyi_kod();
	//_temp_klient();
	return 0;
}