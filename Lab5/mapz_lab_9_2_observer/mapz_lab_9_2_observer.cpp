#include <iostream>
#include <map>
#include <string>
#include <chrono>
#include <thread>
#include <mutex>
#include <vector>
#include <ctime>
#include <iostream>
#include <typeinfo>
#include <iostream>
#include <list>
#include <string>

class temp_sposterihach
{
	public:
		virtual ~temp_sposterihach() {};
		virtual void Update(const std::string& message_from_subject) = 0;
};

class server_chat 
{
	public:
		virtual ~server_chat() {};
		virtual void Attach(temp_sposterihach* observer) = 0;
		virtual void Detach(temp_sposterihach* observer) = 0;
		virtual void Notify() = 0;
	};

class player_chat : public server_chat
{
	public:
		virtual ~player_chat() 
		{
			std::cout << "��� �������.\n";
		}

		void add(temp_sposterihach* observer) override 
		{
			spysok_sp.push_back(observer);
		}
		void remove(temp_sposterihach* observer) override 
		{
			spysok_sp.remove(observer);
		}
		void new_massage() override 
		{
			std::list<temp_sposterihach*>::iterator iterator = spysok_sp.begin();
			show_total_sposterihach();
			while (iterator != spysok_sp.end()) 
			{
				(*iterator)->Update(povidomlennia);
				++iterator;
			}
		}

		void write_text(std::string message = "Empty") 
		{
			this->povidomlennia = message;
			new_massage();
		}
		void show_total_sposterihach() 
		{
			std::cout << "�������i���i� � ��i " << spysok_sp.size() << "\n\n";
		}
	private:
		std::list<temp_sposterihach*> spysok_sp;
		std::string povidomlennia;
};

class Sposterihach : public temp_sposterihach 
{
	public:
		Sposterihach(player_chat& subject) : chat_(subject) 
		{
			this->chat_.add(this);
			std::cout << "����i�, � �������i��� \"" << ++Sposterihach::total_ammount << "\".\n";
			this->number_ = Sposterihach::total_ammount;
		}
		virtual ~Sposterihach() 
		{
			std::cout << "�����, � �������i��� \"" << this->number_ << "\".\n";
		}

		void onovyty(const std::string& message_from_subject) override
		{
			text_from_chat_ = message_from_subject;
			show_last_info();
		}
		void delete_from_total() 
		{
			chat_.remove(this);
			std::cout << "�������i��� \"" << number_ << "\" ���������� �i ������. \n";
		}
		void show_last_info() 
		{
			std::cout << "�������i��� \"" << this->number_ << "\": ���� ���i�������� � ���i : " << this->text_from_chat_ << "\n";
		}

	private:
		std::string text_from_chat_;
		player_chat& chat_;
		static int total_ammount;
		int number_;
};

int Sposterihach::total_ammount = 0;

void klientskyi_kod() 
{
	player_chat* subject = new player_chat;
	Sposterihach* observer1 = new Sposterihach(*subject);
	Sposterihach* observer2 = new Sposterihach(*subject);
	Sposterihach* observer3 = new Sposterihach(*subject);
	Sposterihach* observer4;
	Sposterihach* observer5;

	subject->write_text("����� Ivan1983 ������ ����� 1.2�� !");
	observer3->delete_from_total();

	subject->write_text("����� Ivan1983 ������ ����� 1.2�� :D");
	observer4 = new Sposterihach(*subject);

	observer2->delete_from_total();
	observer5 = new Sposterihach(*subject);

	subject->write_text("����� Ivan1983 ������� ����i");
	observer5->delete_from_total();

	observer4->delete_from_total();
	observer1->delete_from_total();

	delete observer5;
	delete observer4;
	delete observer3;
	delete observer2;
	delete observer1;
	delete subject;
}

int main()
{
	setlocale(LC_CTYPE, "ukr");
	klientskyi_kod();
	return 0;
}