#include <iostream>
#include <map>
#include <string>
#include <chrono>
#include <thread>
#include <mutex>
#include <vector>
#include <ctime>
#include <iostream>
#include <typeinfo>


class shablon_komanda 
{
	public:
		virtual ~shablon_komanda() 
		{
		}
		virtual void Execute() const = 0;
};

class temp_tekstove_pryvitannia : public shablon_komanda 
{
	private:
		std::string temp_loading;

	public:
		explicit temp_tekstove_pryvitannia(std::string pay_load) : temp_loading(pay_load) 
		{
		}
		void do_command() const override 
		{
			std::cout << "�������� �������: ���������� (" << this->temp_loading << ")\n";
		}
};


class otrymuvach 
{
	public:
		void rozpiznaty_ruh(const std::string& a) 
		{
			std::cout << "����i������ ���i� : ��������� (" << a << ".)\n";
		}
		void rozpiznaty_knopku(const std::string& b) 
		{
			std::cout << "����i������ ���i� : ��������� (" << b << ".)\n";
		}
};

class temp_rendering_karty : public shablon_komanda 
{	
	private:
		otrymuvach* otr_1;
		std::string _first;
		std::string _second;
	
	public:
		temp_rendering_karty(otrymuvach* receiver, std::string a, std::string b) : otr_1(receiver), _first(a), _second(b) 
		{
		}
		void do_command() const override 
		{
			std::cout << "����i��� ������� : ��������� ����� ��'���i�\n";
			this->otr_1->rozpiznaty_ruh(this->_first);
			this->otr_1->rozpiznaty_knopku(this->_second);
		}
};

class temp_new_player 
{
	private:
		shablon_komanda* _temp_first_state;
	
		shablon_komanda* _temp_last_state;
	
	public:
		~temp_new_player()
		{
			delete _temp_first_state;
			delete _temp_last_state;
		}

		void Change_first_state(shablon_komanda* command) 
		{
			this->_temp_first_state = command;
		}
		void Change_last_state(shablon_komanda* command)
		{
			this->_temp_last_state = command;
		}
	
		void vhid_gravcia()
		{
			std::cout << "�����: ��i� � ����i\n";
			if (this->_temp_first_state) 
			{
				this->_temp_first_state->Execute();
			}
			std::cout << "�����: �������� �� ����i\n";
			
			if (this->_temp_last_state) 
			{
				this->_temp_last_state->Execute();
			}
		}
};


int main() 
{
	setlocale(LC_CTYPE, "ukr");
	temp_new_player* invoker = new temp_new_player;
	invoker->Change_first_state(new temp_tekstove_pryvitannia("����i���� ������"));
	otrymuvach* receiver = new otrymuvach;
	invoker->Change_last_state(new temp_rendering_karty(receiver, "��� ������", "��� ��i��"));
	invoker->vhid_gravcia();

	delete invoker;
	delete receiver;

	return 0;
}